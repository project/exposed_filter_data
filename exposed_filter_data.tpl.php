<?php

/**
 * @file
 * Basic template file
 */

?>
<div class='exposed_filter_data'>
  <div class='title'>Filtered by:</div>
  <div class='content'>
    <?php
      if ($exposed_filters) {
        foreach ($exposed_filters as $filter => $value) {
          if ($value) {
            print "<div class='filter'><div class='name'>" . $filter . ":</div>";
            print "<div class='value'>" . $value . "</div></div>";
          }
        }
      }
    ?>
  </div>
</div>
